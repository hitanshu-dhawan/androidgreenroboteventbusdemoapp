package com.hitanshudhawan.greenroboteventbusexample;

public class PostEvent {

    private String message;

    PostEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
